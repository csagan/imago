FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /src
EXPOSE 80

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
COPY *.sln ./
COPY Imago/Imago.csproj Imago/
RUN dotnet restore
COPY . .
WORKDIR /src/Imago
RUN dotnet build -c Release -o /src

FROM build AS publish
RUN dotnet publish -c Release -o /src

FROM base AS final
WORKDIR /src
COPY --from=publish /src .
ENTRYPOINT ["imago", "Imago.dll"]